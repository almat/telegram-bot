package commands

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func (c *Commander) List(message *tgbotapi.Message) {
	text := "Products: \n\n"
	products := c.productService.List()

	for _, p := range products {
		text += p.Title + "\n"
	}

	msg := tgbotapi.NewMessage(message.Chat.ID, text)
	c.bot.Send(msg)
}
