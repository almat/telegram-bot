package commands

import (
	"log"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func (c *Commander) Get(message *tgbotapi.Message) {
	args := message.CommandArguments()

	idx, err := strconv.Atoi(args)

	if err != nil {
		log.Println("wrong args", args)
	}

	product, err := c.productService.Get(idx)

	if err != nil {
		log.Println("not found", idx)
	}

	msg := tgbotapi.NewMessage(
		message.Chat.ID,
		product.Title,
	)
	c.bot.Send(msg)
}
